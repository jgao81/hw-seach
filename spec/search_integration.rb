require "selenium-webdriver"
require 'net/http'
require 'json'
require 'uri'

def json_response(endpoint, term)
  url = "#{endpoint}#{term}"
  resp = Net::HTTP.get_response(URI(url))
  result = JSON.parse(resp.body)
  return result
end

require File.dirname(__FILE__) + "/../lib/search_page.rb"

browser = Selenium::WebDriver.for :chrome
search_page = SearchPage.new(browser)
base_url = "https://clutter-front-end-interview.herokuapp.com/"

endpoints = {
  :movies => "movies.json?q[title_cont]=",
  :cast_members => "movies/"
}

describe "search with terms and verify results list" do

  #search_terms = ["bc"]
  search_terms = ["ac","ry","bl","Red","Potter"]

  search_terms.each do |term|
    movies = json_response("#{base_url}#{endpoints[:movies]}",term)
    search_page.search(term)
    l = search_page.complete_movie_list

    it "term #{term} return correct number of items" do
      expect(l.size).to eq(movies.size)
    end

    it "term #{term} movie title match" do
      (0...l.size).each do |i|
        movie_label = "#{movies[i]["title"]} (#{movies[i]["release_date"][0..3]})"
        expect(l[i]).to eq(movie_label)
      end
    end

    it "every movie should cointain term: #{term}" do
      (0...l.size).each do |i|
        expect(l[i].downcase).to include(term.downcase)
      end
    end
  end
end

describe "selecting search item will display details" do
  term = "Harry"

  it "selecting item in results display cooresponding details" do

    search_page.search(term)
    movies = json_response("#{base_url}#{endpoints[:movies]}",term)
    l = search_page.complete_movie_list

    (0...l.size).each do |i|
      search_page.select(i)
      expect(search_page.detail[:title]).to eq(movies[i]["title"])
      expect(search_page.detail[:year]).to eq(movies[i]["release_date"][0..3])
      expect(search_page.detail[:desc]).to eq(movies[i]["overview"])
    end
  end
end
