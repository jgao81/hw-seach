require "selenium-webdriver"
require 'net/http'
require 'json'
require 'uri'

require File.dirname(__FILE__) + "/../lib/search_page.rb"

browser = Selenium::WebDriver.for :chrome
search_page = SearchPage.new(browser)

describe "Smoke Test with pre-populated data" do

  term = "potter"

  it "term potter return correct items" do
    search_page.search(term)
    l = search_page.complete_movie_list
    expect(l.size).to eq(8)
  end

  it "selecting movie shows details" do
  	search_page.select(2)
  	expect(search_page.detail[:title]).to eq("Harry Potter and the Prisoner of Azkaban")
  	expect(search_page.detail[:year]).to eq("2004")
  end
end