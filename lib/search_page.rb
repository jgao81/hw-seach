require "selenium-webdriver"

class SearchPage

	SEARCH_BOX = { css: 'input' }
	RESULT_LIST = { css: 'div > a' }
	SEARCH_STATUS = { css: 'div > span' } 
	RESULT_MORE = { css: 'a:contains("^More$")' }
	RESULT_PREVIOUS = { css: 'a:contains("More")' } 
	DETAIL_TITLE = { css: 'h4' }
	DETAIL_DESC = { css: 'p' }

	attr_reader :driver
	attr_reader :search_box
	attr_reader :status
	attr_reader :list
	attr_reader :detail
	
	def initialize(driver)
		@driver = driver
		visit
		@list = []
		@status = ""
		@detail = {}
		page_loaded?
	end 

	def visit
		driver.get 'https://clutter-automation-interview.herokuapp.com/'
	end

	def select(movie_number)
		if movie_number < list.size
			list[movie_number].click
		end
		sleep 1
		find_detail
	end

	def search(term)
		if search_box.nil? 
			@search_box = driver.find_element(SEARCH_BOX)
		end
		
		search_box.clear
		search_box.send_keys term
		sleep 0.5
		search_complete? while !search_complete?
	end

	def complete_movie_list
		complete_list = []

		if list.size > 10
			while list.size > 10
				complete_list.concat(list[0..9].map {|x| x.text})
				list.last.click
				sleep(2)
				find_list
			end
			complete_list.concat(list.map {|x| x.text} )
			complete_list.pop #pop the last 'previous' button
		else 
			complete_list = list.map {|x| x.text}
		end
		complete_list
	end

	private
		def find_detail
			header = driver.find_element(DETAIL_TITLE).text
			@detail[:title] = header.split("\n")[0]
			@detail[:year] = header.split("\n")[1]
			@detail[:desc] = driver.find_element(DETAIL_DESC).text
		end

		def page_loaded?
			begin
				driver.find_element(SEARCH_BOX).displayed?
				true
			rescue
			  	false
			end
		end

		def search_complete?
			begin
				find_status
				find_list

				status == "No Movies Found for This" || list.size > 0
			rescue
				false
			end
		end

		def find_status
			begin
				@status = driver.find_element(SEARCH_STATUS).text
			rescue
				@status = ""
			end
		end

		def find_list
			begin
				loop do 
					@list = driver.find_elements(RESULT_LIST)
					break if list.last.text != ""
				end
			rescue
				@list = []
			end
		end
end